AncientGuy.Fitness is the journal of an ABOLITIONIST.  

Abolition is about FREEDOM ... and freedom is all about DISCIPLINE ... and the little habits that are about improving the disipline of continuous improvement and expansion of knowledge.

Freedom is never free ... except maybe for the immature, freedom is not something you just inherit and take for granted. Freedom has dues; it has to paid for ... but the inescapable reality is that YOU are the one who must develop your discipline, you cannot purchase this from others, but they will try to sell something that promises this to fools.

Stop EVER blaming others or expecting someone to give you freedom, even when you imagine that you have paid.

Discipline ALL about YOU ... it's not really that much like a bone, it's more flexible and a stength able to adapt, like a muscle that can atrophy ... you are the only one who can strengthen your ability to constantly change and to compound the your self-improvement for YOUR ends.  

There are parasites who will try to get you to fight their battles, pay for their causes, but for anyone, attaining freedom is all about removing one's inefficiencies and the bad habits of being oneself ... when one remove one's bad habits, one not only change oneself but also becomes more efficacious at helping others change themselves.  

***If you want to change the world, you must work HARD every single day at eliminating the self-defeating bad habits of being yourself.***

Change is NEVER EVER given to the weak. NEVER. Anyone who imagines that they are simply entitled to a better world effectively becomes a dangerous, more unhappy, weaponized pawn of those who promise wailing zombies that it's just a matter of zombies following the Fearless Leader ... when you give in to any form of worship of leaders or heros, you cease working on your own heroism and leadership ... unless you develop your discipline of heroism and leadership, you can never be free -- you will only be a wailing, weaponized zombie pawn.

Changing the world is more intensely personal than just an exercise like cleaning your room -- changing the world means that you have to THINK very hard about how difficult change happens IN YOU ... when YOU understand how to change YOU, how to eliminate your bad habits -- then you can start improving your readiness and ability to be more effective at eliminating your bad habits and to help others change themselves. 

If you want to change the world ... forget the utter nonsense about cleaning your room ... change is NOT AT ALL about how something appears -- it's about the FUNCTIONALITY and DURABILITY and FITNESS of something to sustainably make people healthier, freer and less dependendent upon medications or any assistance from others.

At a minimum, this means (in no particular order, because all are simultaneous requirements) you must maintain or work toward:

* No ongoing addiction or long-term dependency upon drugs or presciption medications; If your diet, sleep, fitness or stress mgmt regimen requires medications, it's not healthy.
* A healthy and improvingly healthy diet including knowledge about nutrition, cooking and gardening/farming and where food comes from.
* An active, fit, non-sedentary lifestyle in which chairs, automobiles and other work-saving modern luxuries remain luxuries, but not essential.
* No sleep deficits; a sleep regimen that gives you more than sufficient sleep every day.
* No escapism or dodging problems or difficulties with any form of mental relaxation or entertainment.
* No hysteria or NEED for any of the programming from either hysterics OR the counter-hysterics OR futurolists
* No participationg in lotteries, gambling, casinos or any form of *get rich quick* speculative investing or futurology.

***Every single moment of your Life is a priceless gift.*** There can be no luxury that is better than a day working outside in Nature when you are not under direct attack or threat of impending annhilation. Your working assumption should be that the future will be tougher to figure out, more difficult in ways that you don't understand and just generally HARDER in every dimension ... and harder will be okay, even better! 

AncientGuy.Fitness is logotherapy.  If one exercises the freedom that Viktor Frankl speaks ... in the same way that exercising discipline or training one's *freedom muscles* is like physical exercise, in that the physical exercise is always primarily about the mental aspect of trying a *just bit harder* and doing *one more good rep* when one feels spent ...  then one is always be free to choose how you will react to any set of circumstances.  Logotherapy should be seen as a discipline that one consistently develops every day.  You can be tougher because you are going to work without any let up on being more ready for a more difficult future and you are going to work seriously on the self-control, discipline and time management of staying at that task. The internalization of this appreciation of the magic of Life is the basis of freedom; if you are an abolitionist, you work to make others free by improving the freedom of your own existence.

*** It involves a very simple algorithm ... continually practicing the same ritual to program yourself according to your own logotherapy ***

Practice minimalist Z now, visualize ideal future Z

Be grateful when Life gives any exogenous X

Process, crush, juice, distill, simplify X

Chose endogenous Y to optimize f(X,Y), to perfect next Z

***where*** 

**Z** is the most creative, most disciplined, completely unpretentious, most anonymous lifestyle that you pull off

**Y** is the set of options or endogenous choices that you have available at the time that you react to an exogenous X; Y might be shaped by Z to some degree, but it is about READINESS -- you should neither over-invest in all kinds of just-in-case distractions [ie it's idiotic to prepare for exactly the same combat of the last war; the battle will change] nor ignore pleasant surprises like weather or the particular people in the crowd who happen to show up in an unexpected fashion

**X** is the whole set of things that are completely out of your control -- whether those things are good, bad, indifferent, when life gives you lemons, you can make something lemonny from it