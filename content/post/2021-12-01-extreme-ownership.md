---
title: Extreme Ownership
subtitle: High Agency
date: 2021-12-01
tags: ["TBD0", "CH12"]
---


How do you TRAIN or develop the discipline to exhibit "high agency" or "extreme ownership" when the times comes. How do you build readiness ... not just for ONE kind of fight or ONE kind of challenge, but myriad challenges ... instead of a vaccine that is a silver bullet against just one disease, you are going to need a strong immune response against a variety of diseases, cancers, threats to life, losses of friends/family, sorrows ... in impossible times, how will you OWN the EXTREMELY bad situation.  It's coming.