---
title: One Straw Revolution 
subtitle: Use climate to sculpt your landscape, to sculpt you
date: 2021-07-16
tags: ["TBD0", "CH03"]

---

[The One-Straw Revolution: An Introduction to Natural Farming](https://www.amazon.com/gp/product/1590173139/) or Zen and the Art of Farming” or a “Little Green Book,” by [Masanobu Fukuoka](https://en.wikipedia.org/wiki/Masanobu_Fukuoka) is something of a manifesto about farming and eating but mostly [his journey into natural farming](https://www.permaculturenews.org/2020/07/25/the-philosophy-of-masanobu-fukuoka/) is about the certain, practical, absolute limits of human knowledge and what can be achieved through human effort.

Rather than solve the problems of any pursuit, attempt to do less, to observe, think and understand. Most of the world’s problems arise because of human forced effort, induced chaos and interventions forced upon nature. In Fukuoka's view, it is the ‘extravagance of desire’ which is at the heart of our modern ills, and natural farming is the solution ... although this might sound radical it is, in effect, only repeating the teachings of Christ [in the Sermon on the Mount] or teachings from Buddhism, Taoism or, pretty much, any established SUSTAINABLE cultural tradition ... 

***Happiness comes from EXPERIENCING Nature, from being directly involved in it ... not from as a result of any business or desire to convert it.***

You should try to allow Nature to sculpt you ... *consider the lilies of the field*