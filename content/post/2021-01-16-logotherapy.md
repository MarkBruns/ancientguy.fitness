---
title: Logotherapy
subtitle: The ONLY human freedom
date: 2021-01-16
tags: ["TBD0", "CH01"]

---

You have to know YOUR Why ... unless you are a complete moron who has to follow other people, your Why not going to be someone else's Why -- your Why may not even be at all remotely similar to someone else's Why ... the point of Life is largely about discernment, thinking and discovering exactly what the fucking point is. 

The search for meaning is all about freedom and the expansion of freedoms for others by leading a uniquely meaningful life. Freedom is not free ... and it wasn't paid for by __ INSERT JINGOISTIC NONSENSE HERE __ ... DISICIPLINE equals FREEDOOM ... it is YOUR effort to fight and express your freedom, YOUR moment-by-moment efforts to develop the discipline that will help you fight for the expansion of freedom.

**The ONLY human freedom that matter is the freedom to wake up, assess our surroundings and basically make the most of what is happening.**

Most people are materialist idiots or addicted to comfort-seeking so they cannot begin to beign to ever find the way to exercise this human freedom ... even if people are not actually facing tough circumstances, like Viktor Frankl was in a Nazi death camp, they simply cannot summon the discipline to exercise this freedom ... because they are paralyzed by nostalgia or longing for better, happier time where everything was just so .... and, as a result of this constant longing for something in the rear view mirror, they are effectively DEAD.  

MOST affluent Americans are effectively dead after 25 or 30 ... or maybe even before ... they WON'T start new things, WON'T learn entirely different careers or jobs ... they are entirely unable to make new sets of friends every year.  MOST affluent American adults are effectively just fossilized twentysomethings or teenagers ... who desperately NEED people who they knew five, 10, 25, 50 years ago to be exactly the same DEAD person like they are.

You can tell that MOST affluent Americans are actually dead after 25 or 30 ... because of how UNABLE they are to contemplate significant change.

They are REALLY, REALLY, REALLY slow at learning stuff, so it takes these people a really long time to realize that they stopped living ... as soon as they started looking in the rear view mirror and hoping for their Al Bundy glory days of high school of college.

The world changes rapidly now ... even four or five decades ago, we all sorta know that half-life of any relevant specialized professional skillset was, AT MOST, five years -- which means that in just 25 years ... a professional HAS TO continually train/work-out HARD every day ... like an old school martial artist ... continually replacing / renewing / upgrading / refreshing / re-examining / re-proving / repacking and continually tightening up 97% what they knew ... it's necessary to just tighten shit up and prepare like a motherfucker OR DIE ... to have any chance of staying relevant.

And this is, if anything, actually maybe worse now ... since in all likelihood, the half-life of a professional skillset in 2021 is probably more like two years ... so maybe people are dead at 20 or 25 now ... but death happens and rigor mortis sets in as soon as one's life is dominated by nostaligia.

This same principle applies to friendships, fitness, communities ... EVERYTHING that is living ... within 25 years, 97% of what your thought you knew is JUNK ... if anyone imagined that they knew someone 50 years ago, 99.9% of that should have changed ... UNLESS the person actually died, pretty much stopped living and spent all their time looking in the rear view mirror.

MOST affluent Americans are actually dead after 25 or 30 ... they still swallow crap and turn food into shit ... but they are actually DEAD ... and basically smell like DEATH, to anyone who is still alive.