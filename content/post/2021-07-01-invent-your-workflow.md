---
title: Invent Your Own Workflow
subtitle: Jiujitsu and the Gracie Principles 
date: 2021-07-01
tags: ["TBD0", "CH07"]
---


## ASYNCH.work

[Level up your remote engineering skills](https://about.gitlab.com/blog/2021/03/12/best-practices-remote-engineering/) ... embracing remote-first means NOT embracing meatspace, NOT embracing commute-first. Our REASON for this is that ASYNCH.work allows us to spend more more time in a focused exploration of spiritual growth which, for us, involves a physical jiu jitsu lifestyle with also more time for walking, exploring Nature and the science of immune systems, lifting heavy things, doing landscaping and soil building, tending our flowers.

It is necessary to fully OWN every aspect the choice, don't straddle the fence ... it's not just that physical transport is costly, dangerous and a no-positive-value-added activity ... inconsiderate people assume that their meetings couldn't be accomplished with an email; some of the more recalcitrant will need to smacked in the face to understand that they should not just expect people to show up for a meeting they call. 

ASYNCH.work will not just happen ... it will require you to be more [thoughtful and *paradoxically* more honest [to the point of bluntness] in your communications](https://about.gitlab.com/blog/2021/01/29/tips-for-managing-engineering-teams-remotely/) ... drastically cut down on online meetings, Zoom fatigue or over-exposure, eg Twitter Spaces ... [aggressively, consistently, but patiently cultivate the discipline of working remotely](https://about.gitlab.com/blog/2021/02/09/engineering-teams-collaborating-remotely/); everyone is learning ... RESULTS, RESULTS, RESULTS -- work much harder at BEING [results-driven](https://about.gitlab.com/handbook/values/#results), RATHER than worrying about your socializing ability, ie knowledge of eateries, brewpubs, your professional attire or your vehicle and appearance of status ... 

## Jiu Jitsu Principles

We train in BJJ in order to be more fit, to more ready to be productive/creative ... of course, a big part of it for ancient guys is also training to confront seriously increasing physcial limitations every day. We don't train in order to prove what we alreadu have done; we train in order find and correct weaknesses before those weaknesses kill us. Accordingly, jiu jitsu is fundamentally about discovering humility ... in order to ***LISTEN harder to situations and people who NEED us*** ... focusing upon those who need us requires that we *listen A LOT less to people who constantly just need attention or the kind of people who need us to spread their stories or gossip.*

We TRAIN in order to more thoroughly, deeply, intuitively understand not just techniques, but thirty-two underlying principles articulated by Ryron and Renner Gracie ... but, in a meta-sense, we also train to learn something through interacting [virtually] with the Gracies, [Gracie University](https://www.gracieuniversity.com/Pages/Private/Dashboard) and how the Gracies have developed a sustainable business model in the educational realm ... which no longer is constrained by geography, but now transcends different collections of people across the physical realm. The example of how the Gracies have investigated their market, learned about technologies appropriate to their training and have generally exploited opportunities to reach out adapt to their business environment is an example worthy of study itself.  The martial arts, like all physical education business models, is extremely tough business model to financially sustain.

It is necessary, perhaps to understand something about jiu jitsu is especially important to ancient guys ... the goals are different as one gets older ... it kind of important for the aging person to realize why one will need to refine one's ability as one ages to ***continue*** to execute techniques and drills ... the skill of aging well involves develop different facets of skills to sustain a functional proficiency throughout the aging process. EACH one of the following principles is worthy of at least several weeks of study ... in fact, one will never exhaust new opportunities for discovery by practicing different techniques looking at ways to apply these principles ... to think about and work PRACTICALLY on improving and retaining FUNCTIONAL physical fitness, especially as one ages.

As you grow older, your workflow MUST become a very intensely personal ART ... just as the martial arts should be evaluated as an ART, to help you to adapt creatively, artistically to your very personal unique aging process, ie it is the ART that is yours and that you love that will help you discover why you simply must do your art every day -- it is not going to be either fear OR some obtuse financial argument that you cannot afford financially to not roll every day.

EACH one of the following principles could be expanded into a book ... but, in their own way, EACH one is only a hint of what is covered in the material and discussed in the fora -- hopefully, these small exammples illustrate why you absolutely must have a direct [even if via the virtual / online realm]connection to training with the Gracies, to [Gracie University](https://www.gracieuniversity.com/Pages/Private/Dashboard) and others who roll with the Gracies.

### Connection
The first principle illustrates the DEPTH of the ART in each one of the Gracie's [32 principles](https://www.gracieuniversity.com/Pages/Public/Subject?enc=f9IIAuzld8tGV%2fqc5P8QFg%3d%3d). Like every principle, the [Connection principle](https://www.gracieuniversity.com/Pages/Public/Lesson?enc=Aaj7v1MUMQcY65Fdx4EHpw%3d%3d) helps you to see all jiu-jitsu techniques through an entirely different lens or from an entirely different angle -- studying this principle for several weeks to gain this different perspective really helps to see the connections between a lot of dots that might have been just an isolated bag of tricks. EACH one of these 32 principle is intended to be explored, in depth, as a matter of focus, for several weeks.

Jiu jitsu is a language, a form of kinesthetic communication using your body to express your martial art, in preventing, promoting or predicting your opponent’s movement using the 32 different methods of connection through the: 1) hands, 2) fist, 3) wrist, 4) forearm radius, 5) forearm ulna, 6) elbow, 7) elbow pit, 8) biceps, 9) triceps, 10) shoulder, 11) armpit, 12) neck, 13) head, 14) chin, 15) chest, 16) back, 17) stomach, 18) ribcage, 19) hips, 20) glutes, 21) quadriceps, 22) hamstring, 23) knee) 24) shin, 25) knee pit, 26) calf, 27) heel, 28) ankle, 29) sole of foot, 30) instep, 31) toe tops, 32) bottom of toes. Learning a language requires practice ... and articulating the language lesson for other fellow students who are  practicing the language. These 32 different physical methods cut across ALL of the different techniques learned in jiu jitsu. In this training, Renner and Ryron illustrate some examples to make the point effectively why one needs to look at all techniques with this perspective ... these connecting methods are like letters of the alphabet which form syllables, words, sentences, paragraphs, stories, books, libraries, civilizations -- it is so fundamental to learn the language of connections.

### Detachment
Optimizing efficiency in transitions by deliberate disconnection from your opponent.
### Distance

### Pyramid

### Creation

### Acceptance
The Acceptance principle is about being the first to accept the inevitablity of an action so that you are best prepared for using the remaining energy that is moving toward the outcome.

### Velocity

### Clock

### River

### Frame
Using the power of the frame or primarily skeletal structure and alignment rather than muscular strength. Framing or proper structural alignment is a lifestyle, throughout the entire 24 hours of the day, every day.
### Kuzushi
[Kuzushi](https://en.wikipedia.org/wiki/Kuzushi) is the Japanese term for different ways of unbalancing an opponent in the martial arts.
### Reconnaissance
EVERY single little touch or exchange, regardless of outcome, is an opportunity to observe and learn ... and, then to learn more, to explore with different techniques [often as nothing other than feints or distractions] to add more depth to the exchange ... which helps to observe and learn even more about your opponent that you can later use.

### Prevention
Tease, harry, disrupt your opponent's progress toward their objective. The goal is provoke anger, confusion and overreaction ... in order to prevent the opponent's progress. ***Trust*** the escape, ensure survival ... as long as you are in control, preventing the opponent's success ... the opportunity to attack and submit an opponent will come.

### Tension

### Fork
The fork principle has a lot of prerequistes and depends upon knowledge of what has works, what doesn't -- which is another ways of saying that it a way for ancient guy to use experience. Creating positional dilemmas that forces your opponent to choose how they lose ... but it also reveals something about how they think. A forked position creates to or more "prongs" of opportunity, which in turn can create more prongs of opportunity or ... at some point ... an easy submission is possible.

### Posture
Every technique has an optimal or preferred [starting] position or posture. Doing good jiu jitsu involves exploiting circumstances to use a strong posture to initiate a technique. Thus, a strong defense involves frustrating the mental comfort of the opponent, ie be like the pebble in the shoe, continually derailing mental focus by taking away opportunities for setting up these preferred postures.

### False Surrender
Feigning surrender so that your opponent lets their gaurd down. Doing this can lure your opponent into making a move that you are prepared to capitalize on.
### Depletion
Draining your opponent's physical, mental and spiritual energy using targetted actions and connections. The key to survival is energy efficiency, you want to burn energy at the slowest rate. The rate of energy depletion is usually the key in fights that are remotely close to being equal ... it's possible to frustrate the opponent with no expenditure of energy, sticking hands and hugging, slowly sautéing and pestering, hanging on the opponent and using anything to provoke excess use of energy and generally deplete the spiritual energy, control the tempo and win the fight.
### Isolation
Tactically containing one or more of your opponent's limbs for your [temporary] advantage.
### Sacrifice
Chess 101 ... giving up something of actual or percieved value to gain a tactical advantage in another form. When progress is absolutely not possible in conventional methods it may be possible to sacrifice something in order to change the match. 
### Momentum
Capitalizing on mass in motion in order to maximize energy efficiency against your opponent. Momentum can be used to facilitate movement while conserving energy in a fight.
### Pivot
Increasing the effectivness of a technique by changing its angle of application.
### Tagalong
Free rides are everywhere in jiujitsu. Use free rides in order to deplete your opponent's energy while saving your own.
### Overload
Disproportionate application of your resources to target a specific point on your opponent's body.
### Anchor

### Ratchet

### Buoyancy

### Head Control

### Redirection

### Mobility

### Centerline

###  Grandmaster