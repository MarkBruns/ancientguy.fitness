---
title: Learn How To Learn
subtitle: What you know about learning is insufficent, you need to continually learn how to improve your ability to learn how to learn
date: 2021-10-01
tags: ["TBD0", "CH10"]
---


Learning new things requires failing, perhaps publicly, uncomfortably ... and getting really frustrated to the point of tears because you can't figure out how something works.

Learning is TOUGH ... if it's not -- you're probably doing it wrong; you're probably imagining that you are learning things, when in reality you're just comfortably watching or listening to a favorite old professor make you feel enlightened and aware ... but actually LEARNING things is necessarily HARD and most likely involves pain, discomfort, unease -- or else, if you don't feel the stress and aggravation, your brain synapses are probably not being rewired.

However, learning is NOT masochism ... it's that HARD things to learn involve pain ... it's NOT that pain alone teaches you anything.