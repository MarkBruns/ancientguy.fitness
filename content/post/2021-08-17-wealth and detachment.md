---
title: Ask not, what your country can do for you ...
subtitle: ... ask how you can dogfood the improvement of everyone's productivity-as-code
date: 2021-08-17
tags: ["TBD0", "CH05"]

---

*Realize right now, if you are reading this, you are already inately wealthy ... STOP indulging your inner needy, affluent consuming parasite ... stop asking what others can do for you, instead ...* 

## ASK WHAT YOU CAN DO FOR THE IMPROVEMENT OF EVERYONE'S CREATIVITY.

Regard your stock of wealth as all of the assets within your reach that can make you productivve and creative. 

You don't have to be just a tourist, a swallower, a drunk, an addict ... a mere ***consumer***.

**You CAN create** ... if you choose to create. You can build your own tools, resources and assets which help you to create and, MOST IMPORTANTLY, you can build that stock of wealth for everyone else.  Moreover, that stock of infrastructural wealth can help to advance the cause of all sentient being in the your Creator's universe. 

You can engineer, develop and improve that infrastructure-as-code OR you can just be a mere ***consumer***.

When you ***[dogfood](https://en.wikipedia.org/wiki/Eating_your_own_dog_food)*** your creativity or productivity infrastructure, you can share it and dogfood everyone's infrastructure ... actually taking advantage of wealth is optional; it requires work, effort and failure. 

By contrast ... pathetic, consuming parasites are completely self-centered; they only ONLY know how to complain and be jealous of others. ***Pity the parasites.***

The increase in the value of your [dogfooded](https://en.wikipedia.org/wiki/Eating_your_own_dog_food) productivity-as-code increases the stock of everyone's shared wealth. Thus, only idiots are acquisitive or worried about their OWN wealth. Your main source of wealth must primarily be your prouctivity knowledge asset which you must continually maintain, refactor, simplify, build, improve and put to use upon in service as the greater good.

*If you want to be genuinely wealthy, you will need a code or set RULES which you should use in governing your life, in order to not be an impoverished, parasitic, whining fool who is worthy only of pity.*

1) Recognize RIGHT NOW that you will never ever need anything that you cannot get on your own or demand [and kill for, when necesssay] from others, ie the right to breathe, to think or have control over one's body. You have this wealth-as-code; now improve upon it.
   
2) Know your WHY, but think lightly of yourself and your role in that WHY. Do think deeply of your mission or WHY the Creator has placed you in the world at this point in time. It is not random. 

3) Be detached from desire in everything, throughout every moment of your whole life. Regard any lapse into desire as a pathetic failure. Never desire or expect applause or fame; stay away from those who demand applause, moments of fame and any stroking of their ego.
    
4) Do not regret what you have done or how you have failed. Celebrate each opportunity to learn by exploit each failure or regret to the fullest.

5) Never EVER EVER be envious, covetous jealous. Regard any time that you have pathetically either hated success or desired the wealth  of others with great shame.

6) Accept the blessing of each moment and appreciate everything just the way it is. Understand and be grateful but NEVER go out seeking thrills, pleasure or experiences for their own sake.

7) Never let yourself be saddened by a physical separation or loss; realize the blessing of having just the ideas, memories and best parts without the burden of either caring for someone or witnessing their suffering and struggle.

8) Resentment and complaint are appropriate neither for oneself nor others. Let go of it. Resentment and complaint are really nothing but another form of envy and jealousy.

9)  NEVER let yourself be guided by your FEELINGS of lust or love or hate or fear. Recognize and respect feelings for what they are, but pity those who are still pathetically misled by these things.

10) In ALL things, have no preferences. Instead APPRECIATE differences. Refuse to participate in any form of epicurean chauvenism.

11) Be entirely indifferent to where you live. Work at being more mobile and adaptable. Be able to transcend circumstance, to live anywhere.

12) NEVER pursue the taste of good foods, intoxicants, entertaining distractions or greater comforts ... instead appreciate nourishment, vigorous exercise and rhythm of your breathwork.

13) Holding on to possessions or assets that you no longer need is strictly a matter of lugging baggage and maintaining a burden. You are not going to need the burden as much as you need mobility.
    
14) NEVER follow customary beliefs simply because they are customary; most human activity is the result of continuation of bad habits -- success and influence come from replacing mindless habits with mindful habits.
    
15) Do not collect weapons or practice with weapons beyond what is useful. Drive out your fear of violence or death by focusing what you will do to your enemies, rather than worrying about what your enemies will do to you.
    
16) Do not seek to possess either goods or assets for your old age ... seek to obtain the poise, fearlessness and self-respect of one who needs absolutely nothing.
    
17) Respect ancient wisdom, but fear God. Constantly look to your Creator for insight, wisdom and knowledge of the right thing to do at every moment.

18) As you age, you will grow frail, it will be necessary to abandon your expectations of what your body/mind are capable of. Senile men imagine nonexistent greatness or capacities of youth. Great men are humble. With greater humility, preserve your adherence to principles, rather than appearances.

19) Do not stray from the path ... but when you do, repent, forgive yourself and then earnestly pray for the wisdom to avoid straying from the way but do not forget that ALL stray ... ALL must be humble ... only idiots imagine that they are perfect.
    
20) Avoid entanglements and the commitments of others. Do not move ahead with anything based upon only a partial feeling. If it is worth doing, COMMIT to moving ahead and do not look back. However, NEVER accept the commitments reflecting the manipulative expectations of others.