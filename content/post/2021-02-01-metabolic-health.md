---
title: Metabolic Health
subtitle: You might imagine that you are independent, except that you generally become what you consume
date: 2021-02-01
tags: ["TBD0", "CH02"]
---

## Insulin Resistance

Bikman, et al

## Appetite Management

Fung, et al

Hydration -- drink filtered water, but mostly, just drink more water

## Outdoor Exercise

Lift, walk, punch, dig ... move them move more ... 5S to move

Get outdoors -- heliotherapy for [vitamin D](https://www.connectedpapers.com/main/3466766690228569ad426dc687b521336e39e4ef/Antimicrobial-implications-of-vitamin-D/graph)

## Stress / Endorphine Management

Seek eustress

Pay attention to endorphines, eg avoid soy. Hofmelker, et al


