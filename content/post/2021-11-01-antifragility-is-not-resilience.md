---
title: Antifragility is NOT resilience
subtitle: Antifragile preparation is NOT about having a backup generator; it's about being acclimated and ready enjoy a break, spending time in Nature during a power outage.
date: 2021-11-01
tags: ["TBD0", "CH11"]
---


Antifragiliy is not resilience; it's a much more aggressive, yet less exposed risk management posture that is entirely about benefitting from chaos ... there's enough chaos in the world that you don't need to create more, BUT you can take positions or TRAIN or set yourself up to be ready, fit, acclimated to be in a position to benefit from disruptions in the status quo, changes in climate, tectonic shifts.  
