---
title: Contribute Skills, Not Assets
subtitle: A data-driven world does not respect your collection of stuff
date: 2021-05-01
tags: ["TBD0", "CH05"]
---

***The data-driven virtual realm is MINIMALIST*** ... it does not need your collection of great books, useful computer parts or the old lawnmowers and vehicles in your garage -- the data-driven world is not interested in anything in your underwear drawer. That might be obvious, but what might not also be obvious is that also doesn't really give a damned about your money or your estate bequests, it'll just use money in order to fundraise to get more money -- the data-driven world does not care about YOUR money ... it might care about YOU. 

**With the exception of land, the data-driven world is greatly ACCELERATING the REAL depreciation of most assets.**  

You need to understand what that rapid acceleration means. Most people do not yet get that their precious STUFF, which might be useful to OR adored by them OR their *pride and joy*, is worth basically LESS THAN nothing to other people ... and that happens before BEFORE the package of new stuff be delivered by UPS; ie most people's packages are not even close to being worth the valuable time of thieves to bother stealing those packages ... there's way more money for the criminal mind in data crime AND being able to sell infosec skills to data-aware people who have money and are justifiably worried about data crime.  *Only the STUPID criminals take/pawn physical stuff, ie if they'd been to prison university for criminals, they'd know that already.*  Stuff is junk that someone will have to carry and dispose of.  

***The data-driven virtual realm is MINIMALIST*** ... which means the only value in anything, with the exception of farmland, is at it's immediate point of consumption. If it sits, it rots and depreciates faster than ever. 

Yes, there are exceptions, but the exceptions tend to mislead and confuse people ... this confusion is especially true for those senile elderly folks who recall something that happened in 1981 as if it were only yesterday ... no, it's 2021 -- 1981 should be like five or six lifetimes or career pursuits or marriages ago. 1981 is NOT yesterday; you simply cannot be the same person that you were in 1981 -- unless you are some sort of calcified fossil or stone. Those stuck in the past or to much too fondly attached to the old Dodge Charger R/T they had as their first car in high school to think straight ... but not thinking straight directly determines one's ability to know that one is not thinking straight -- so senile old codgers really need question whether they should take their keys away. Old fossilized shit might be collected by old fossils ... but to everyone else who does not understand the neat fossilized patterns and how those patterns evoke memories of the first time in the back seat, it's just old shit.

Some collectibles MIGHT hold their value ... but MOST collectibles, which the owner might love and enjoy -- require expensive care, maintenance/repairs, storage and increasingly heftier commissions to the few world-class auctioneers able to liquidate a menagerie of high end collectibles. Not all owners of collectibles really understand that it is the exclusive niche market LARGELY CREATED BY THE AUCTIONEERS that is responsible for a collectible, eg antique muscle cars or classic antique imports, which drives the value of everything in that collectible ecosystem, eg barn finds are worth something because some of the uber-wealthy like Jay Leno have a nice garages ... but the fascination with automobiles might be a fashion, popular now, but possibly like the fascination with china/glassware in housewives after WW II, and not as big in 50 years. 

As a general rule, even the real estate which is used to sell STUFF to STUFF-addicts, like big box retailers and commercial real estate is not going to be worth much of anything until AFTER it is repurposed for a data-driven world.


EFFECTIVE venture philanthropy is about skills ... INEFFECTIVE, parasitic venture philanthropy is about money and fundraising ... if you want to make a difference, contribute RELEVANT skills building open source things that people WILL need in the next five to ten years. 

Those in need will benefit from the ephemeral tools that your skills will build ... for example, much technology is still, right now, going into making markets and logistics work better.  To a VERY SMALL degree, better markets through smart contracts explains some of the interest in fintech and the techologies behind crytocurrencies BUT ... much of the recent rise is not really about the underlying smart contract technology. Instead it's about the highly speculative hope of the stampeded herd animals that something they don't understand will be worth more in two months than it is now.  As general rule, better information and commication technologies result in much more efficient exchanges/commerce networks -- this will continue to help 7.5 billion people in all corners of the world radically improve their standards of living. 

As we have seen in the last 20 years, these "people at the bottom" will sell their labor for higher amounts in larger, more globally-connected markets than what the local exploiters offer them ... AND, on the consumption side, those larger, more globally connected markets will also help people find better things more efficiently for much less cost than what is provided by local exploiters.

## Acquisitive materialists are going to suffer

***The data-driven virtual realm is MINIMALIST*** ... which means BETTER INFORMATION ... local exploiters cannot rely on scarce information to overcharge ... and assets depreciate much more rapidly ... ultimately, this means that people who are acquisitive materialists are free to continue being acquisitive materialists ... but they are going to pay more for that freedom AND when they can't change their habits or reel in their desires, they going to suffer and profoundly DESERVE to suffer in the data-driven virtual realm. 

Assets matter much less than skills in the virtual realm. Being grateful for unusual comforts is DIFFERENT than the entitled, ungrateful addiction to the comforts of material wealth. Addiction makes thinking, learning and adapting MUCH MORE difficult ... thus, material wealth and particularly the desire for status and material wealth is almost entirely baggage.

In affluent America, we see fewer ultra-productive, ambitious scramblers/hustlers/builders of new wealth and more fearful, but entitled asset holders watching their dollar-denominated accounts rapidly dwindle in real value ... affluent addicts are terrible at adapting to change -- one of their maladaptions is speculating in bitcoin in a pathetically futile attempt to win in a zero sum game and accumulate more wealth before the next guy.  

The data-driven virtual realm is MINIMALIST and increasingly LEAN and AGILE ... This is why healthier alternative to investing in assets is investing time/energy in skills and understanding something about which skills are going to be needed to better solve future problems, to dramatically increase the future stock of knowledge and to develop networks with other who are doing things, building skills, providing leadership and genuine long-term, sustainable compassion. 

## The smart talent of the world has been / is / will continue emigrating to the virtual realm.  

 This might scare people; they might think something like "That's not for me; I have no background in the interwebs."
 
 NOBODY had any.  NOBODY is born with a background.  As you grow older, your background becomes an EXCUSE for not learning -- you have to BREAK THE HABIT of making this excuse.
 
 Your background will probably not be anywhere close the virtual realm ... not too many people have background's there. That's how it works.  It's like people emigrating to America in the 19th century ... NOBODY in their circle of family and friends had a background in what it meant to build out the infrastructure of a country like America ... NOBODY ever has background in putting the infrastructure in place in a brand new frontier ... but smart talent seeks the opportunity where smart talent will be the most productive in the future ... that is why the emigration is underway to the virtual realm ... and away from the maintenance-intensive, commute-intensive WASTE of the physical bricks-and-mortar realm.  

 Unless you are facing more or less immediate death, you should be thinking about reinventing your life right now ... because, just in case you have missed it, the world has changed more dramatically in the last five years than in any five years during your life. NOTE: This assumes people reading this were not alive during WW II and cognizant of the global events. It is true that Spring 1940-1945 was a bigger global change, but WW II is really the ONLY five year period in the last 100 that compares with the silent technological change of 2016-2021. 
 
 You might have to learn things, but you really should try to understand the AI/ML ops landscape [or start contemplating your admittance to a managed eldercare facility] ... at a minimum, you should be weakly conversant in a ML/data ops language like Python ... weakly conversant means being able to almost follow an adult conversation like a curious toddler.
 
 The following are the general kinds of ML concepts that you with want to look into at a high level in order to understand conversations in Python ... you don't NEED to know any of these, it's a list of what kinds of things are involved in using Python language to do data science and machine learning -- in order to have some comprehension of what is possible RATHER THAN LISTEN TO FEARMONGERING FROM CLUELESS JOURNALISTS, you should begin to experiment with Python, Jupyter notebooks, GitHub/Gitlab and the realm of various data sets in the cloud ... this isn't going to make you competetent -- it's more like taking a course in VisiCalc on an Apple IIe at the local community college back in 1981.  As the early VisiCalc courses gave people an awareness of what could be done with computers -- exercises in Python, Jupyter notebooks, GitHub/Gitlab and the realm of various data sets furnish a hands-on awareness and de-mystification of artificial intelligence and machine learning.
 
 For the uninitiated, a good way to get started is to install and play around with [the Anaconda Individual Edition data science platform](https://docs.anaconda.com/anaconda/) along with the [Conda package manager](https://conda.io/en/latest/)... eventually you will want to start get familiarity with some of the more important areas in Data Science and [Machine Learning Operations, including monitoring](https://learning.oreilly.com/library/view/introducing-mlops/9781492083283/) before wading into what can quickly become expensive if not closely monitored ...and you might want to understand something about the infrastructure driving adoption before you dive into adoption yourself, ie learn from the experience of others. It is not just computation power or scaleable access to that power in the cloud, but the open source Machine Learning libraries like [TensorFlow](https://www.tensorflow.org/) applied in the algorithms of a program like [AlphaGo Zero](https://en.wikipedia.org/wiki/AlphaGo_Zero) for better utilization of that power.

 ![Computation-powered adoption driven by the infrastructure for utilization](https://learning.oreilly.com/library/view/engineering-mlops/9781800562882/image/B16572_01_01.jpg)


• [Concurrency and multi-threading in Python](https://stackoverflow.com/questions/2846653/how-can-i-use-threading-in-python)



• [Functional programming](https://docs.python.org/dev/howto/functional.html), [Enum](https://docs.python.org/dev/howto/enum.html) and [other HOWTOs](https://docs.python.org/dev/howto/)

• The [Data model](https://docs.python.org/dev/reference/datamodel.html), [Data Classes](https://docs.python.org/dev/library/dataclasses.html), meta classes and special [object methods](https://www.w3schools.com/python/gloss_python_object_methods.asp) like dunder methods
 

• [Logging](https://docs.python.org/3/howto/logging.html), the general approach behind different [logging patterns or recipes](https://docs.python.org/3/howto/logging-cookbook.html) [monitoring](https://learning.oreilly.com/topics/monitoring/), ESPECIALLY IMPORTANT [when one gets to the point where one wants to exploit greater computing power and cloud computing resources for data science and machine learning] is a solid awareness of [monitoring and improving performance of machine learning models](https://learning.oreilly.com/videos/monitoring-and-improving/9781491988855)

• [Hashability](https://stackoverflow.com/questions/14535730/what-does-hashable-mean-in-python#14535739) and [merging two dicts](https://www.python.org/dev/peps/pep-0584/) [in a single Python expression](https://stackoverflow.com/questions/38987/how-do-i-merge-two-dictionaries-in-a-single-expression-taking-union-of-dictiona/26853961#26853961). A [hash function](https://en.wikipedia.org/wiki/Hash_function) is any function that can be used to map data of arbitrary size to fixed-size values. A mutable container, such as a list or dictionary, is used to maps keys to values allowing efficient retrieval of mutable values by immutable object keys. Hash functions are related to (and often confused with) checksums, check digits, fingerprints, lossy compression, randomization functions, error-correcting codes, and ciphers. Although the concepts overlap to some extent, each one has its own uses and requirements and is designed and optimized differently. The hash functions differ from the concepts numbered mainly in terms of data integrity.

• Itertools, [iterators](https://docs.python.org/3/tutorial/classes.html#iterators) [iterables](https://stackoverflow.com/questions/9884132/what-exactly-are-iterator-iterable-and-iteration), [generators, a kind of iterable you can only iterate over once](https://stackoverflow.com/questions/231767/what-does-the-yield-keyword-do/231855#231855) and the yield keyword function that returns a generator.  Don't confuse your [Iterables, Iterators, and Generators](https://stackoverflow.com/questions/9884132/what-exactly-are-iterator-iterable-and-iteration) or [generator expressions vs list comprehensions](https://stackoverflow.com/questions/47789/generator-expressions-vs-list-comprehensions)



• In general, all tips, tricks, traps, clever algorithms or design patterns that have something to do with exception handling or pattern matching are obviously something that warrant extra study, ie the time will be repaid over and over ... for example, a [regular expression or regex or regexp](https://www.regular-expressions.info/) is a sequence of characters that specifies a search pattern ... Python has a built-in module called [**re** ... *short for regular expression*](https://www.w3schools.com/python/python_regex.asp) which has its own metacharacters, special sequences, sets, functions... 



• [Type hints](https://stackoverflow.com/questions/32557920/what-are-type-hints-in-python-3-5) involve the [gradual typing](https://www.youtube.com/watch?v=2wDvzy6Hgxg). Since inferring or checking the type of an object being used is especially hard in Python due to its dynamic nature ... it's really important to have an understanding of the [general theory behind type hints](https://www.python.org/dev/peps/pep-0483/).


• [Closures](https://stackoverflow.com/questions/tagged/closures) 


• Slicing and [understanding why slice notation is used](https://stackoverflow.com/questions/509211/understanding-slice-notation)


• [Lambdas](https://stackoverflow.com/questions/tagged/lambda) ... not the AWS service, but the anonymous functions or closures in programming languages such as Lisp, C#, C++, Lua, Python, Ruby, JavaScript, or Java.


• Context managers

• Enumeration

• Async I/O and programming

• *args, **kwargs
• map(), filter(), reduce()
• Ternary Operators
• Coroutines
• Enumerate
• Patterns for data structures / algorithms
• Test-driven programming
• Other modules beyond numpy or pandas