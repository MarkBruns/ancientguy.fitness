---
title: Make 160 friends, influence people
subtitle: There are 8 billion people on the planet; it's okay if only 160 are your true friends.
date: 2021-04-01
tags: ["TBD0", "CH04"]
---


Back in Dale Carnigie's day, if one wanted to be a success as an insurance agent or real estate broker, it was important to build friendships with EVERYONE indiscriminately and to run around the area TIRELESSLY with a happy, optimistic face being a LOCAL community booster ... communities were, and still are, built by the networkers who are genuine in their concern for people.

But the world has changed significantly in that networking is no possible with any of a couple of billion people who have access to the interwebs ... the significance of this cannot be overstated ... it's now possible, maybe imperative, that you network with those people who are INTERESTED in the kinds of things that INTEREST you ...

Geography no longer constrains networks ... thus, you MUST choose ... and you can choose not to choose, but your choice WILL define you ... to a lesser, but still significant degree, you will be defined by how well you have developed your **asynchronous** communication skills, because others are on different clocks than you are -- your work schedule has NOTHING to do with someone else's work schedule now -- so you must accept that you are going to need to really COMMUNICATE well asynchronously. Or else.

There are people who you cannot help who want to have their information broadcast to them OR they want to be entertained or coddled by a friend who never challenges them, but the world has changed -- it's not the 1980s any more. There are zombies who still need mass communication programming from their teevee, radio or favorite YouTube channels ... but communication is now a billion-way street ... your use, mastery, sharpening, tool-building of knowledge tools like [ConnectedPapers](https://www.connectedpapers.com/main/505580e498e700c65cb6ea808aa3a85f7e57490b/Understanding-SARSCoV2Mediated-Inflammatory-Responses-From-Mechanisms-to-Potential-Therapeutic-Tools/graph) or [SemanicScholar](https://www.semanticscholar.org/topic/Immune-response/466) will help you to be aware of the literature and data in your field, be aware of code repositories and data sets ... but you still need to establish human friendships and communicate with other friends who "get you" as you do the same.

You simply cannot even try to influence old thinkers who mentally belong in the nursing home ... even if you try to be nice -- it will be like communicating with your grandparents when you were in college learning things. You will just piss off old people who want to be old and go back to living in their 20s again.  You can be compassionate, kind, patronizing to old thinkers ... you can still be an "old friend" but you can't be a current friend, and you surely can't influence them.

If you actually want to be a good friend ... even a good "old friend" to people who you don't really talk to any more ... you will need to recognize here's a serious upper bound to how many people you can really be FRIENDS with ... and it might have to change.