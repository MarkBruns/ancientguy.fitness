---
title: LISTEN
subtitle: The term “observability” was coined by engineer Rudolf E. Kálmán in 1960 ...
date: 2021-09-01
tags: ["TBD0", "CH09"]
---


Kalman's paper [On The General Theory of Control Systems](https://www.sciencedirect.com/science/article/pii/S1474667017700948) is of monumental significance ... along with Shannon's work on the theory of information and that the value is information is gaged by how it CHANGES operational working assumptions (ie, you can safely avoid all programming that only confirms operational working assumptioin with NO LOSS IN VALUE) ... it would be safe to say that, Kalman's basic ideas about on observability fundamentally structure how we view ALL intelligence gathering ... Kalman's paper immensely helps us with general framework of thinking that we need such that we can begin to answer the question, *"What kind and how much information is needed to achieve a desired type of control?"* ... obviously, the kind of people how dismiss a paper like Kalman's 1960 are the kind of people who can safely be entirely ignored from then on.

In Section 6 of the paper, Kalman introduces the concept of observability and solves the problem of reconstructing unmeasurable state variables from the measurable ones in the minimum possible length of time. In Section 7, he formalizes the similarities between controllability andobservability by means of the Principle of Duality and goes on to show that the Wiener filtering problem is the natural dual of the problem of optimal regulation ... the seemingly obviously conclusion is that a system is observable only to the extent that it is controllable ... it's a corrolarry of Shannon, information can have value ONLY if it can be used to CHANGE operational working assumptions ... otherwise, it's like the sound of the tree falling to earth in the forest -- the sound of the falling tree might have huge value to the living things there, but for those who will never visit or care about the forest, it's just extra noise that gets in the way of information that has operational value. 

## Human listening skills

In order to build the DISCIPLINE of listening, you should focus on just one skill in listening leadership at a time ... the time that you spend working on discipline every single day will be repaid many times over in terms of more efficient, more efficacious listening skills. One of the most important skills in listening is learning who and what can safely be avoided with no loss of value whatsoever.  In a manner similar to a workout, building better habits and discipline of listening depends upon actually working out, working on something that will add value and fitness, ie it's better to do functional physical work than it is to spend time being coddled on an exercise machine. Although you might want to give yourself points for driving to and showing up at the gym, it's likely that mere gym membership is a complete waste of time ... in comparison to FUNCTIONAL physical work.  So the best way to build the discipline of listening is to PAY ATTENTION to what happens during the day, eg when you are outside doing landscaping, digging, pulling weeds, pay attention to what the birds/squirrels are *saying,* try to listen to your heart and breath, work at LISTENING to what is happening NOW, ditch the nasty habit of wanting to be constantly entertained ... when you go in a store, OBSERVE everything in that store. When you are around people, OBSERVE everything about them, including body langauage ... when someone does something that is inherentdly offensive or inconsiderate, observe to understand the weakness driving it.  *OBSERVE as if you were in prison.*

## Listening to programmers means READING their code.

We WRITE ... and review our code LATER ... after it's gone cold, to LISTEN to ourselves.

The follow are core guideline for good code -- you might see examples of this AND, perhpas more importantly, you should notice the ANTI-patterns of less-than-perfect code..

[1] Don’t panic! All will become clear in time ... is there more evidence of patience OR impatience.

[2] Don’t use the built-in features exclusively or on their own. On the contrary, the fundamental (built-in) features are usually best used indirectly through libraries, such as the ISO C++ standard library..

[3] You don’t have to know every detail of C++ to write good programs. You don't have to write perfect programs to write useful programs -- you have to start somewhere.

[4] Focus on solid programming techniques and structure, not on language features or cleverness.

[5] Writing good code will present good opportunities to review the state of the language definition issues in the current ISO C++ standard and debate on proposed changes.

[6] “Package” meaningful operations as carefully named functions; name functions and generally structure code for maintainability -- you will come back to it.

[7] A function should perform a single logical operation.

[8] Keep functions short; then review, refactor and make good functions even shorter.

[9] Use overloading when functions perform conceptually the same task on different types.

[10] If a function may have to be evaluated at compile time, declare it constexpr.

[11] Understand how language primitives map to hardware; understand the hardware.

[12] Use digit separators to make large literals readable.

[13] Avoid complicated expressions.

[14] Avoid narrowing conversions.

[15] Minimize the scope of a variable.

[16] Avoid “magic constants”; use symbolic constants.

[17] Prefer immutable data.

[18] Declare one name (only) per declaration.

[19] Keep common and local names short, and keep uncommon and nonlocal names longer.

[20] Avoid similar-looking names.

[21] Avoid ALL_CAPS names.

[22] Prefer the {}-initializer syntax for declarations with a named type.

[23] Use auto to avoid repeating type names.

[24] Avoid uninitialized variables.

[25] Keep scopes small.

[26] When declaring a variable in the condition of an if-statement, prefer the version with the implicit test against 0.

[27] Use unsigned for bit manipulation only.

[28] Keep use of pointers simple and straightforward.

[29] Use nullptr rather than 0 or NULL.

[30] Don’t declare a variable until you have a value to initialize it with.

[31] Don’t say in comments what can be clearly stated in code.

[32] State intent in comments.

[33] Maintain a consistent indentation style.