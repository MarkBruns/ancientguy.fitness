---
title: Spiritual health
subtitle: Not a possession or asset, but more of a journey or pilgrimage.
date: 2021-01-01
tags: ["TBD0", "CH01"]

---

You must love how your Creation came into being; respect and honor your Creator. 

The most fundamental thing about honoring your Creator is that you must acknowledge the distinction between being responsible for creating your life with your life AND the originating act of Creation.  You can exercise, sculpt and shape your own unique -- you were created to do that, but you did not bring your existence and everything upon which it depends into being. You are not God. You cannot become God by merely studying books, even what you define as the "inspired Word of God" -- you are not qualified to be God's buddy, to know God's mind OR, especially, to judge others according to your necessarily worthless understanding of others or your Creator. 

***Respecting and honoring your Creator means living in continually-inspired AWE and fear of God.***  You can acquire the power and social capital in order to browbeat others, to use peer-pressure, gossip and manipulation to control other people [temporarily] ... in fact, the devil is more than accomodating when it comes to assisting capable, but wayward souls in "getting their white knight on" to *help* and *guide* other people ... heavy doses of pride and ambition are ladeled into the cups of those who would try to be equal to God or to speak for God. *God does not EVER need your assistance in reaching the masses.*

Anyone who has helped teach people how to think about problem-solving and experimental design will recognize rapidly that not one of us really a clue about how other humans think ... we have clues about the lowest common denominator thinking that is necessary to communicate ideas or share concepts ... but humans are not remotely close to being identical. Your Creator did not mass manufacture a bunch of widgets or even complex gadgets -- stop treating your unique life as if you are someone's gadget.  When you fail to respect your unique soul, you will suffer for destroying the most beautiful treasures of your Creator.  Respect that which is bringing you and other beings into these UNIQUE existence ...  respect your unique soul; if you struggle with this, work harder at respecting the unique souls of others. 

In order to have a better understanding of what you can and should become, you must look forward.  Consider what we know from driving safely -- it does not make any sense to focus upon the rear view mirror, unless we are  backing up *and even then, it makes sense to get out and look when it matters* ... but you must drive going forward.  Your thinking about the road ahead is about where you are going to drive -- it's not about driving someone else's car although you should pay attention to where the traffic is flowing. It is up to you to change the bad habits of being yourself -- you waste your time and damage your soul when you blame your suffering on the bad habits of others. 

***Do NOT look back. You are not going that way.***  

EXCESSIVE emotional attachment to the past or re-living past relationships or second-guessing oneself on which path was taken is unhealthy ... you can determine what is EXCESSIVE; however, you probably KNOW better than anyone else if you are still fixated on an -ex or traumatized by some operation that went wrong decades ago. Obviously, cherished memories of special events or special people matter to all of us. There is a place for the study of History AFTER the data and understanding is old enough that the personal attachment or longing for the past is not a factor.   of course, some reflection upon memories IS involuntary and natural, but cultivation of a longing for a past which is not going to return is pathological, especially in a spiritual sense.

History is that old or distant enough that it is beyond emotional attachment is something that IS necessary to understand. The lessons that were learned by others will be applicable in the future. This is why you should deepen your study of your own traditions as you study and respect the traditions of your fellow humans. But these lessons are like the lessons one observes or experiences directly by living IN Nature. 

Understanding is mostly about the nuerotic fixations that you remove, rather than your favorite stories or that what which you are neurotically fixated upon. The path to spiritual health is found through cultivating compassion, deepening the love of your Creator and discerning your Creator's will ... earnest prayer, fasting and abstinence will take . 

The tradition that you were born into was not random, not a mistake ... there's a reason for that, just as there is a reason for your curiosity, your desire to explore and test, your exposure to other traditions. The notion of a randomness in statistics or probability theory can be a useful analytical construct when we lack the omniscience to fathom the depth and breadth of everything acting upon a situation ... but NOTHING in the Universe is random. 

Therefore, anyone who is opposed to an individual's pursuit for clarity of understanding and independence from the domination or peer pressure of groups will necessarily find what I have to say very infuriating ... for example, gossiping and social pressure is way that idiots and bullies attempt to force others into lowest common denominator thinking in which the gossip or bully uses the group to attack the person with independent views -- thus, gossips and bullies live in a hellish world in which they always feel persecuted and, frankly, they deserve the hell that they create.

**Mindfulness or spiritual health is a VERY, VERY, VERY LONG PILGRIMAGE ... a pilgrimage that makes a trip to Mecca seem trivial ... it is the ordinariness of the pilgrimage that matters.  It is not exactly something, even an idea, that one possesses but rather it is a general sense of health and ability to travel that one continually works toward ... through different dimensions.**

Mindfulness might be seen as JUST slowing down to think or thinking through the *next moves on the chessboard* ... but there's a little more to spirital health than just momentary or temporary mental clarity.  

Obviously, there's ALSO much, much, much more to spiritual health than endulging distractions into religion, [annoying] pedantic knowledge of scripture or theology or being able to regurgitate what some favorite author said about Christianity or any other religion. Christianity is justly criticized for being about churches, peer pressure, false piety, old-fashioned [as opposed to newer forms of] virtue signalling and, just generally being IRRELEVANT as all fuck to ALL people who have serious, misunderstood spiritual fears or worries.

Christianity is like any other professionally-prescribed pill, not really dangerous in itself, but dangerous when people CHOOSE to rely upon the drug or the euphoia of music or the wonderous sense of epiphany ... rather than attempting to try to LIVE ordinarily, in an infinitely sustainable, low- or zero-cost healthy, minimalist manner with no material distractions to alter or impose upon one's commitment to *journeying* indefinitely.

Spiritual health must be LIVED ... or a path that must be travelled.  Even though that metaphor is more than just a bit stale -- cliches become cliches because they are apt.  The spiritual journey must be enjoyed and celebrated privately [not Pharisaically-shared with people who, for a variety of good reasons, just don't want to hear about that happy horseshit].  

Otherwise, spirituality just translates into human copy of what anyone could read in a book, but most people choose not to because it's just tedious, impractical, moralistic virtue-signalling.  

Your spiritual health comes out of YOUR lived life ... although that life might be informed by something you read, the most important thing [as it is with physical health/fitess or professional knowledge] is to really LIVE, to USE one's spiritual health and to push yourself to BEYOND one's limits in the daily EXERCISE of one's faith. At some point, it is necessary to transcend the cartoon figures living in Flatland who [rightfully FEAR what they don't know but choose to remain dead] but yet feel compelled to believe and prosteletize ridiculous things about some OTHER supernatural realm ... it's not really a matter of abandoning them; it's still necessary to respect and pray for them, BUT ... there are some things one can't fix.  

It's as in one's own life -- being told what to do is no substituted for having LIVED it.  Spiritual health is NOT about piety or preaching -- it is about LIVING [anonymously or not famous for being a "good Christian"] in the world of prisons, developmentally-challenged learners or the vast realm of imperfect humans with other ideas about Life. 

Living is about deliberately, mindfully, perhaps prayerfully thinking about life ... for example, LIVING the Lord's Prayer is fundamentally about acknowledging a lack of knowledge [of heaven or higher realms], feeling a sense of gratitude, eschewing all forms of acquisitivensss and worry about material things, requesting wisdom and courage while acknowledging fallibilty ... it's possible to LIVE the Lord's prayer while breathing and doing yoga, hiking [preparing for pilgrimage], grounding one's self in ordinary chores or gardening, camping or deliberately enduring some form privation, contributing time/effort to open source, meditation or prayerful expression of kindness, fasting or doing without something while carrying on with normal activities, [pilgrimage](https://spark.adobe.com/page/N9XrnoRGek4v1/) or longer expedition or finally retreating to a more reclusive, monastic existence. 

# Distinguishing Pilgrimage or Spiritual Health From Spiritual Shopping Or Tourism

Spiritual health or pilgrimage is always about the ordinary aggravation of WORKING on one's faith or insight -- spiritual shopping or tourism is like televison programming or some other shiny nuance, profundity or neat factoid for the enlightened.


Spiritual health involves a workout or exercise or the heavy-lifting of penance or pondering search for wholeness ... spiritual shopping or tourism is about consuming or maybe having something like a drink to numb one's awareness.


Spiritual health is necessarily solitary in some respects, just as with the benefit from weightlifting acrues to one who lifts the weight, but the healthy spiritual person should be open to all conversations, arguments, phone calls, messages WITHOUT ever needing to "call a meeting" or to "hold a church service" to enforce one's views -- spiritual shopping or tourism is limited to only like-minded people, friends, family, those with a chosen favorite interest, eg a book club.

Spiritual health or pilgrimage is about the silence [even in a crowd] to of an internal sacred space or introspective personal dialogue	with the soul ... spiritual shopping or tourism does not allow for an internal sacred space unless that space is shared with the entire group of select, like-mined people who are present and all following the spiritual "leader"


Spiritual health depends upon the private daily [or more frequent] repetition of a very ORDINARY ritual to drive the internalization of some sort of personally-desired change in habit ... spiritual shopping or tourism depends upon the public weekly or seasonally repetition of a very special or fantastic ritual to wow the audience and drive fear and awe.


Spiritual health depends upon private, hidden, daily philanthropy and contribution to things that benefit everyone, but especially those who cannot afford to pay ... spiritual shopping or tourism depends upon checkbook charity, guilt and very public fundraising efforts in with the congregants are shamed into giving more than they should to large ostentatious cathedrals constructed to benefit future peer pressuring and fundaising efforts.


Spiritual health depends upon total commitment to the ordinariness of the journey and the realization that pilgrimage is a push to be closer to one's Creator and can never really be over for a human being ... spiritual shopping or tourism is a continual stream of evangelical rush parties, special revivals and "feasts" of great speakers which are tied to the euphoric special memories of holidays or vacations that just "end too soon" when people have to go back into the ordinary real world.