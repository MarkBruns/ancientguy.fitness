---
title: Metabolic Health
subtitle: You might imagine that you are independent, except that you generally become what you consume
date: 2022-02-01
tags: ["TBD0", "CH02"]
---

## Moderation

If you look at much of the literature ... maybe a few thousand articles ... or something that covers that material such as this [September 2021 Review of Alcohol's Impact on the Cardiovascular System](https://www.connectedpapers.com/main/c318c7ec4da3f464acfde1bb53327c2b695877a2/Alcohol%E2%80%99s-Impact-on-the-Cardiovascular-System/graph) ... you come away with the conclusion that alcohol's effect upon health is definitely mixed ... there is definitely a J-shaped curve in which the total abstainers don't fare as well as the moderate consumers of alcohol ... but **moderate means MODERATE** -- no extra indulgence; certainly no bingeing ...when someone drinks more than one glass of wine at a time and alcohol consumption turns detrimental FAST